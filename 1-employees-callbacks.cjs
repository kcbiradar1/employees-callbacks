const fs = require("fs");

function readFile(path, callback) {
  fs.readFile(path, "utf-8", (err, data) => {
    if (err) {
      callback(Error("Error occured while reading file!!!"));
    } else {
      callback(null, data);
    }
  });
}

function writeFile(path, data, callback) {
  fs.writeFile(path, JSON.stringify(data), (err, data) => {
    if (err) {
      callback(Error("Error occured whiler creating file!!!"));
    } else {
      callback(null, data);
    }
  });
}

function doAllOperationsFunctions() {
  readFile("data.json", (err, data) => {
    if (err) {
      console.error(err.message);
    } else {
      let store_data = JSON.parse(data);
      const query_array = [2, 13, 23];
      const query_result = store_data["employees"].reduce(
        (previous, current) => {
          if (query_array.includes(current.id)) {
            previous.push(current);
          }
          return previous;
        },
        []
      );
      writeFile("public/output/query_data.json", query_result, (err) => {
        if (err) {
          console.error(err.message);
        } else {
          console.log("Query output stored successfully!!!.");
          const data_based_on_company = store_data["employees"].reduce(
            (previous, current) => {
              if (previous[current.company]) {
                previous[current.company].push({
                  id_of_employ: current.id,
                  name_of_employ: current.name,
                });
              } else {
                previous[current.company] = [];
                previous[current.company].push({
                  id_of_employ: current.id,
                  name_of_employ: current.name,
                });
              }
              return previous;
            },
            {}
          );
          writeFile(
            "public/output/data_based_on_company.json",
            data_based_on_company,
            (err) => {
              if (err) {
                console.error(err.message);
              } else {
                console.log("Details stored based on company!!!");
                const target_comapny = "Powerpuff Brigade";
                const comapy_detail = store_data["employees"].filter(
                  (each_company) => {
                    return each_company.company === target_comapny;
                  }
                );
                writeFile(
                  "public/output/company_details.json",
                  comapy_detail,
                  (err) => {
                    if (err) {
                      console.error(err.message);
                    } else {
                      console.log("Company details recovered successfully!!!.");
                      let new_store_data = store_data["employees"].filter(
                        (each_employee) => {
                          return each_employee.id !== 2;
                        }
                      );
                      writeFile(
                        "public/output/data_after_deletion.json",
                        new_store_data,
                        (err) => {
                          if (err) {
                            console.error(err.message);
                          } else {
                            console.log(
                              "The id with 2 is deleted successfully!!!"
                            );

                            new_store_data.sort((emp1, emp2) =>
                              emp1.company > emp2.company
                                ? 1
                                : emp1.company == emp2.company
                                ? emp1.id > emp2.id
                                  ? 1
                                  : -1
                                : -1
                            );
                            writeFile(
                              "public/output/sorted_data.json",
                              new_store_data,
                              (err) => {
                                if (err) {
                                  console.error(err.message);
                                } else {
                                  console.log(
                                    "The sorted data is stored successfully!!!"
                                  );
                                  let store_postion = [];
                                  for (let index in new_store_data) {
                                    if (
                                      new_store_data[index].id == 93 ||
                                      new_store_data[index].id == 92
                                    ) {
                                      store_postion.push(index);
                                    }
                                  }
                                  let temp = new_store_data[store_postion[0]];
                                  new_store_data[store_postion[0]] =
                                    new_store_data[store_postion[1]];
                                  new_store_data[store_postion[1]] = temp;

                                  writeFile(
                                    "public/output/swap_data.json",
                                    new_store_data,
                                    (err) => {
                                      if (err) {
                                        console.error(err.message);
                                      } else {
                                        console.log(
                                          "id's with 93 and 92 is swapped successfully!!!"
                                        );
                                        let modified_data_added_birth_date =
                                          new_store_data.reduce(
                                            (previous, current) => {
                                              if (
                                                parseInt(current.id) % 2 ==
                                                0
                                              ) {
                                                const add_birth_date = {
                                                  id: current.id,
                                                  name: current.name,
                                                  DOB: `${new Date()}`,
                                                };
                                                previous.push(add_birth_date);
                                              } else {
                                                previous.push(current);
                                              }
                                              return previous;
                                            },
                                            []
                                          );

                                        writeFile(
                                          "public/output/modified_birth_date_added.json",
                                          modified_data_added_birth_date,
                                          (err) => {
                                            if (err) {
                                              console.err(err.message);
                                            } else {
                                              console.log(
                                                "Birth date is added for with even id'!!!"
                                              );
                                            }
                                          }
                                        );
                                      }
                                    }
                                  );
                                }
                              }
                            );
                          }
                        }
                      );
                    }
                  }
                );
              }
            }
          );
        }
      });
    }
  });
}
doAllOperationsFunctions();
